<?php

/**
 * @file Menu Taxonomy Views definitions.
 */

/**
 * Implement hook_views_data().
 */
function menu_taxonomy_views_views_data() {
  $data = array();

  $data['menu_taxonomy']['table']['group'] = t('Menu Taxonomy');
  $data['menu_taxonomy']['table']['join'] = array(
    'taxonomy_term_data' => array(
      'handler' => 'views_join',
      'left_field' => 'tid',
      'field' => 'tid',
    ),
  );
  $data['menu_taxonomy']['mlid'] = array(
    'title' => t('Menu link id'),
    'help' => t('Unique menu item identifier.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
  );
  $data['menu_taxonomy']['tid'] = array(
    'title' => t('Term id'),
    'help' => t('The term id, menu-sensitive.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'menu_taxonomy_views_argument_taxonomy',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'taxonomy_term_data',
      'base field' => 'tid',
      'label' => t('term id'),
    ),
  );

  return $data;
}

function menu_taxonomy_views_views_data_alter(&$data) {
  $data['menu_links']['table']['join']['menu_taxonomy'] = array(
    'left_field' => 'mlid',
    'field' => 'mlid',
  );
  $data['menu_links']['table']['join']['taxonomy_term_data'] = array(
    'left_table' => 'menu_taxonomy',
    'left_field' => 'tid',
    'field' => 'tid',
  );

  $data['menu_links']['tid'] = array(
    'title' => t('Term id'),
    'help' => t('The term id, menu-sensitive.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'menu_taxonomy_views_argument_taxonomy',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['menu_links']['taxonomy_mlid'] = array(
    'title' => t('Taxonomy Menu link id'),
    'help' => t('The unique taxonomy menu link identifier.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'menu_taxonomy',
      'base field' => 'mlid',
      'relationship field' => 'mlid',
      'label' => t('taxonomy menu link id'),
    ),
  );
}
